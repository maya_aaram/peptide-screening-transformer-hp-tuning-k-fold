# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, division
from io import open
import unicodedata
import string
import re
import random
from datetime import datetime
from pathlib import Path
import shutil

import pandas as pd
import numpy as np
from sklearn.metrics import r2_score , mean_squared_error
import matplotlib.pyplot as plt

from pyGPGO.covfunc import matern32
from pyGPGO.acquisition import Acquisition
from pyGPGO.surrogates.GaussianProcess import GaussianProcess
from pyGPGO.GPGO import GPGO


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint

######################################################################################################
######################################################################################################
class AAs:
    def __init__(self, name, sequences):
        # The constructor expects a name and a list of strings.
        self.name = name
        self.aa2index = {}
        self.aa2count = {}
        self.index2aa = {}
        self.n_residues = 0  # start as empty dict
        #print("Counting amino acids ...")
        for seq in sequences:
            self.addSequence(seq)

    def addSequence(self, sequence):
        for aa in list(sequence.upper()):
            self.addResidue(aa)

    def addResidue(self, aa):
        if aa not in self.aa2index:
            self.aa2index[aa] = self.n_residues + 1
            self.aa2count[aa] = 1
            self.index2aa[self.n_residues] = aa
            self.n_residues += 1
        else:
            self.aa2count[aa] += 1
    
    def FASTA2indices(self, seq, max_length = 70):
        indices = [0] * max_length
        try:
            for i, aa in enumerate(list(seq.upper())):
                indices[i] = self.aa2index[aa]
            return indices
        except:
            #print(f"Something didn't work when converting {seq} to indices.")
            return None

######################################################################################################
######################################################################################################
"""
## Implement multi head self attention as a Keras layer
"""

class MultiHeadSelfAttention(layers.Layer):
    def __init__(self, embed_dim, num_heads=8):
        super(MultiHeadSelfAttention, self).__init__()
        self.embed_dim = embed_dim
        self.num_heads = num_heads
        if embed_dim % num_heads != 0:
            raise ValueError(
                f"embedding dimension = {embed_dim} should be divisible by number of heads = {num_heads}"
            )
        self.projection_dim = embed_dim // num_heads
        self.query_dense = layers.Dense(embed_dim)
        self.key_dense = layers.Dense(embed_dim)
        self.value_dense = layers.Dense(embed_dim)
        self.combine_heads = layers.Dense(embed_dim)

    def attention(self, query, key, value):
        score = tf.matmul(query, key, transpose_b=True)
        dim_key = tf.cast(tf.shape(key)[-1], tf.float32)
        scaled_score = score / tf.math.sqrt(dim_key)
        weights = tf.nn.softmax(scaled_score, axis=-1)
        output = tf.matmul(weights, value)
        return output, weights

    def separate_heads(self, x, batch_size):
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.projection_dim))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, inputs):
        # x.shape = [batch_size, seq_len, embedding_dim]
        batch_size = tf.shape(inputs)[0]
        query = self.query_dense(inputs)  # (batch_size, seq_len, embed_dim)
        key = self.key_dense(inputs)  # (batch_size, seq_len, embed_dim)
        value = self.value_dense(inputs)  # (batch_size, seq_len, embed_dim)
        query = self.separate_heads(
            query, batch_size
        )  # (batch_size, num_heads, seq_len, projection_dim)
        key = self.separate_heads(
            key, batch_size
        )  # (batch_size, num_heads, seq_len, projection_dim)
        value = self.separate_heads(
            value, batch_size
        )  # (batch_size, num_heads, seq_len, projection_dim)
        attention, weights = self.attention(query, key, value)
        attention = tf.transpose(
            attention, perm=[0, 2, 1, 3]
        )  # (batch_size, seq_len, num_heads, projection_dim)
        concat_attention = tf.reshape(
            attention, (batch_size, -1, self.embed_dim)
        )  # (batch_size, seq_len, embed_dim)
        output = self.combine_heads(
            concat_attention
        )  # (batch_size, seq_len, embed_dim)
        return output


######################################################################################################
######################################################################################################
"""
## Implement a Transformer block as a layer
"""
class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super(TransformerBlock, self).__init__()
        self.att = MultiHeadSelfAttention(embed_dim, num_heads)
        self.ffn = keras.Sequential(
            [layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim),]
        )
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, inputs, training):
        attn_output = self.att(inputs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)

######################################################################################################
######################################################################################################
"""
## Implement embedding layer

Two seperate embedding layers, one for tokens, one for token index (positions).
"""
class TokenAndPositionEmbedding(layers.Layer):
    def __init__(self, maxlen, vocab_size, emded_dim):
        super(TokenAndPositionEmbedding, self).__init__()
        self.token_emb = layers.Embedding(input_dim=vocab_size, output_dim=emded_dim)
        self.pos_emb = layers.Embedding(input_dim=maxlen, output_dim=emded_dim)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions
######################################################################################################
######################################################################################################
def prepareData(filename):
    pairs = pd.read_csv(filename)
    AA = AAs("Amino Acids", pairs["FASTA"].tolist())
    #print("Counted amino acids:", AA.name, AA.n_residues)
    return AA, pairs
######################################################################################################
######################################################################################################
def train_k_fold(embed_dim = 32, ff_dim = 32, num_heads = 2, eps=0.01, dro = 0.1, epochs = 1000, k = 8, batch_size = 10):
    embed_dim = int(round(embed_dim))*12
    num_heads = int(round(num_heads))
    ff_dim = int(round(ff_dim))
    batch_size = int(round(batch_size))
    vocab_size = 21
    summary = pd.DataFrame({"Fold": ["Best_training_RMSE", "Best_validation_RMSE", "test_RMSE", "test_R2"]})
    test_data = np.array_split(data, k)
    out_dir = root_results_dir + "/" + datetime.now().strftime("%Y-%m-%d__%H-%M-%S")
    Path(out_dir).mkdir(parents=True, exist_ok=True)
    mean_test_RMSE = 0
    mean_test_R2 = 0
    HPT_results = open(root_results_dir + "/HPT.txt", "a+")
    results = open(out_dir + "___training_curves.txt", "a+")
    
    for i in range(k):
        print(f"\nFold {i+1}\n")
        train_val = data[~data.index.isin(test_data[i].index)] # Put test set aside
        train_val = train_val.sample(frac=1).reset_index(drop=True) # Shuffle, reset indices
 
        X_tr = np.asarray(keras.preprocessing.sequence.pad_sequences(list(train_val["encoded_FASTA"]), maxlen = maxlen))
        Y_tr = np.asarray(list(train_val["MIC_against_Salmonella(µM)"]))

        X_test = np.asarray(keras.preprocessing.sequence.pad_sequences(list(test_data[i]["encoded_FASTA"])))
        Y_test = np.asarray(list(test_data[i]["MIC_against_Salmonella(µM)"]))

        """
        Transformer layer outputs one vector for each time step of our input sequence.
        Here, we take the mean across all time steps and use a feed forward network on top of it to classify text.
        """

        inputs = layers.Input(shape=(maxlen,))
        embedding_layer = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim)
        x = embedding_layer(inputs)
        transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim)
        x = transformer_block(x)
        x = layers.GlobalAveragePooling1D()(x)
        x = layers.Dropout(dro)(x)
        x = layers.Dense(20, activation=tf.nn.leaky_relu)(x)
        x = layers.Dropout(dro)(x)
        outputs = layers.Dense(1)(x)
        model = keras.Model(inputs=inputs, outputs=outputs)
        
        saved_model_name = out_dir + f'/best_model__fold_{i+1}_of_{k}.h5'
        
        es = EarlyStopping(monitor='val_loss', mode = 'min', verbose=1, patience=20)
        checkpoint = ModelCheckpoint(saved_model_name, monitor='val_loss', mode='min', save_best_only=True, save_weights_only=True)

        ## Train and Evaluate
        model.compile("adam", loss='mse', metrics=[tf.keras.metrics.RootMeanSquaredError()])
        train = model.fit(X_tr, Y_tr, batch_size=batch_size, epochs=epochs, validation_split=0.15, callbacks=[es, checkpoint], verbose = 0  )
        results.write(f"training_RMSE__Fold_{i+1}/{k}," + ",".join(str(item) for item in train.history["root_mean_squared_error"]) + "\n")
        results.write(f"validation_RMSE__Fold_{i+1}/{k}," + ",".join(str(item) for item in train.history["val_root_mean_squared_error"])  + "\n\n")
        
        best_train_score = min(train.history["root_mean_squared_error"])
        best_val_score   = min(train.history["val_root_mean_squared_error"])
        # Test time:
        model.load_weights(saved_model_name)
        test  = model.evaluate(X_test, Y_test, verbose=0, return_dict = True, batch_size = batch_size)
        test_RMSE = test["root_mean_squared_error"]
        mean_test_RMSE += test_RMSE
        Y_predict = model(X_test)
        test_R2 = r2_score(Y_predict, Y_test)
        mean_test_R2 += test_R2
        summary[f"{i+1}"] = [best_train_score, best_val_score, test_RMSE, test_R2]
        print(f"Train RMSE = {best_train_score}  Val RMSE = {best_val_score}   Test RMSE {test_RMSE}  Test R2 = {test_R2}\n")
        del model

    shutil.rmtree(out_dir)
    results.close()
    summary.to_csv(out_dir+ "___summary_of_results.csv", index=False)
    HPT_results.write(f"embed_dim = {embed_dim}, ff_dim = {ff_dim}, num_heads = {num_heads}, eps = {eps}, "\
                + f"dropout = {dro}, batch_size = {batch_size}, test_RMSE = {mean_test_RMSE/k}, test_R2 = {mean_test_R2/k}\n")
    HPT_results.close()
    return -mean_test_RMSE/k        # pyGPGO maximizes the function, so return the error times -1

######################################################################################################
######################################################################################################
maxlen = 70

AA, data = prepareData("03__Salmonella__clean_convert.csv")
data["encoded_FASTA"] = data["FASTA"].apply(AA.FASTA2indices)
data.dropna(axis = 0, inplace = True)
data.reset_index(inplace = True, drop = True)

root_results_dir = "HPT__" + datetime.now().strftime("%Y-%m-%d__%H-%M-%S")
#train_k_fold(embed_dim = 80, ff_dim = 32, num_heads = 4, eps=0.01, dro = 0.1, epochs = 1000, k = 8, batch_size = 10):
#test_RMSE = train_k_fold(epochs = 2, k = 2, batch_size = 10)

cov = matern32()
gp = GaussianProcess(cov)
acq = Acquisition(mode='UCB')
#acq = Acquisition(mode='ExpectedImprovement')
param = {
		'embed_dim': ('int', [2,8]),
		'ff_dim'   : ('int', [10, 100]),
		'num_heads': ('int', [1,4]),
		'eps': ('cont', [0.0005, 0.1]),
		'dro': ('cont', [0.00001, 0.7]),
		'batch_size': ('int', [5, 40])
		}
#np.random.seed(168)
gpgo = GPGO(gp, acq, train_k_fold, param)
gpgo.run(max_iter=5000,init_evals=3)


