# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function, division
from io import open
import unicodedata
import string
import re
import random

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torch.utils.data import TensorDataset , Dataset, DataLoader
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import pandas as pd
import numpy as np
from sklearn.metrics import r2_score , mean_squared_error
import matplotlib.pyplot as plt

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

######################################################################################################
######################################################################################################
class AAs:
    def __init__(self, name, sequences):
        # The constructor expects a name and a list of strings.
        self.name = name
        self.aa2index = {}
        self.aa2count = {}
        self.index2aa = {}
        self.n_residues = 0  # start as empty dict
        print("Counting amino acids ...")
        for seq in sequences:
            self.addSequence(seq)

    def addSequence(self, sequence):
        for aa in list(sequence.upper()):
            self.addResidue(aa)

    def addResidue(self, aa):
        if aa not in self.aa2index:
            self.aa2index[aa] = self.n_residues + 1
            self.aa2count[aa] = 1
            self.index2aa[self.n_residues] = aa
            self.n_residues += 1
        else:
            self.aa2count[aa] += 1
    
    def FASTA2indices(self, seq, max_length = 70):
        indices = [0] * max_length
        try:
            for i, aa in enumerate(list(seq.upper())):
                indices[i] = self.aa2index[aa]
            return indices
        except:
            print(f"Something didn't work when converting {seq} to indices.")
            return None

######################################################################################################
######################################################################################################
class PepDS(Dataset):
    def __init__(self, X, Y, l):
        self.X = X
        self.y = Y
        self.l = l
        
    def __len__(self):
        return len(self.y)
    
    def __getitem__(self, idx):
        return self.X[idx], self.y[idx], self.l[idx]
######################################################################################################
######################################################################################################
class LSTM_regr(torch.nn.Module) :
    def __init__(self, vocab_size, embedding_dim, hidden_dim1, hidden_dim2, num_layers=1, dr_o = 0.1, batch_first=True ) :
        super().__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_dim, padding_idx=0, max_norm = 1)
        self.lstm = nn.LSTM(embedding_dim, hidden_dim1, batch_first=True)
        self.linear1 = nn.Linear(hidden_dim1, hidden_dim2)
        self.linear2 = nn.Linear(hidden_dim2, 1)
        self.dropout = nn.Dropout(dr_o)
        
    def forward(self, x, l):
        x1 = self.embeddings(x)
        #print("\n\n",x, "\n#################\n", x1)
        x = self.dropout(x1)
        x_pack = pack_padded_sequence(x, l, batch_first=True, enforce_sorted=False)
        lstm_out, (ht, ct) = self.lstm(x_pack)
        xx= F.relu(self.linear1(ht[-1]))
        return self.linear2(xx)
######################################################################################################
######################################################################################################
######################################################################################################
######################################################################################################
def prepareData(filename):
    pairs = pd.read_csv(filename)
    AA = AAs("Amino Acids", pairs["FASTA"].tolist())
    print("Counted amino acids:", AA.name, AA.n_residues)
    return AA, pairs
######################################################################################################
def train_model_regr(model, epochs=10, lr=0.001):
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adam(parameters, lr=lr)
    #optimizer = torch.optim.RMSprop(parameters, lr=lr, momentum = 0.5)
    val_rmse_list =[]
    val_r2_list = []
    train_loss_list = []
    for i in range(epochs):
        model.train()
        sum_loss = 0.0
        total = 0
        for x, y, l in train_dl:
            x = x.long()
            y = y.float()
            optimizer.zero_grad()
            y_pred = model(x,l)
            #print(y_pred)
            loss = F.mse_loss(y_pred, y.unsqueeze(-1))
            loss.backward()
            optimizer.step()
            sum_loss += loss.item()*y.shape[0]
            total += y.shape[0]
        #for name, param in model.named_parameters():
        #    if param.requires_grad:
        #        print (name, param.data)
        train_loss_list.append(sum_loss/total)
        val_r2, val_rmse = validation_metrics_regr(model, val_dl)
        val_rmse_list.append(val_rmse)
        val_r2_list.append(val_r2)
        if i % 1 == 0:
            #import pdb; pdb.set_trace()
            plt.plot(np.arange(i+1), train_loss_list, label="train")
            plt.plot(np.arange(i+1), val_r2_list, label="val_r2")
            plt.plot(np.arange(i+1), val_rmse_list, label="val rmse")
            plt.legend()
            plt.savefig('plots/c.png', bbox_inches='tight', pad_inches=0.1, dpi = 300)
            plt.clf()
            plt.close()
            print("Train MSE =  %.3f  Val RMSE = %.3f   Val_R2 = %.3f" % (sum_loss/total, val_rmse, val_r2))

#######################################################################################################
#######################################################################################################
def validation_metrics_regr (model, valid_dl):
    model.eval()
    correct = 0
    total = 0
    sum_loss = 0.0
    Y = []
    Y_hat = []
    for x, y, l in valid_dl:
        x = x.long()
        y = y.float()
        Y.append(y)
        y_hat = model(x, l)
        #import pdb; pdb.set_trace()
        Y_hat.append(y_hat)
    return r2_score(Y,Y_hat) , np.sqrt(mean_squared_error(Y_hat, Y))
#######################################################################################################
#######################################################################################################
######################################################################################################
#######################################################################################################


AA, data = prepareData("03__Salmonella__clean_convert.csv")
data["encoded_FASTA"] = data["FASTA"].apply(AA.FASTA2indices)
data["length"] = data["FASTA"].apply(len)
data.dropna(axis = 0, inplace = True)
data.reset_index(inplace = True, drop = True)

train_data = data.sample(frac=0.90, axis = 0, random_state = 88)
val_data = data[~data.index.isin(train_data.index)]

train_data.reset_index(inplace = True, drop = True)
val_data.reset_index(inplace = True, drop = True)

X_train = torch.tensor(list(train_data["encoded_FASTA"]))
Y_train = torch.tensor(list(train_data["MIC_against_Salmonella(µM)"]))
l_train = torch.tensor(list(train_data["length"]))

X_val = torch.tensor(list(val_data["encoded_FASTA"]))
Y_val = torch.tensor(list(val_data["MIC_against_Salmonella(µM)"]))
l_val = torch.tensor(list(val_data["length"]))

train_set = PepDS(X_train, Y_train, l_train)
val_set = TensorDataset(X_val, Y_val, l_val)

train_dl = DataLoader(train_set, batch_size=10, shuffle=True)
val_dl = DataLoader(val_set)

model =  LSTM_regr(vocab_size = 21, num_layers = 1, embedding_dim = 38, hidden_dim1 = 16, hidden_dim2 = 37, dr_o = 0.5)
train_model_regr(model, epochs=5000, lr=0.003)


